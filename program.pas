uses matrix;

var
    matrix : SquareMatrix;
    max_element : MatrixElement;
begin
    Writeln('��������� �������� �������');
    GenerateRandomMatrix(matrix);
    Writeln('������� �������');
    PrintMatrix(matrix);
    Writeln('������ ��������');
    max_element := GetMaxSecondaryDiagonalElement(matrix);
    Writeln('������������ ������� � ������ ', max_element.row, ' ������� ', max_element.column, ' �� ��������� ', max_element.value);
    Writeln('������ ������� ��������������');
    max_element.value := Trunc(GetLowerMainDiagonalAverrage(matrix));
    Writeln('������� ��������������: ', max_element.value);
    Writeln('������� �������');
    ReplaceElement(matrix, max_element);
    Writeln('������� �������');
    PrintMatrix(matrix);
    Readln();
end.