unit matrix;

interface

const
    MatrixSize = 6;

type SquareMatrix = array [1..MatrixSize, 1..MatrixSize] of Integer;

type MatrixElement = record
    column : Integer;
    row : Integer;
    value : Integer;  
end;

procedure GenerateRandomMatrix(var matrix : SquareMatrix);

procedure PrintMatrix(var matrix : SquareMatrix);

function GetMaxSecondaryDiagonalElement(var matrix : SquareMatrix) : MatrixElement;

function GetLowerMainDiagonalAverrage(var matrix : SquareMatrix) : Real;

procedure ReplaceElement(var matrix : SquareMatrix; element : MatrixElement);

implementation

procedure GenerateRandomMatrix(var matrix : SquareMatrix);
const
    MinRandom = -6;
    MaxRandom = 4;
var
    i, j: Integer;
begin
    for i := 1 to MatrixSize do 
    begin
        for j := 1 to MatrixSize do
        begin
            matrix[i,j] := random(MinRandom,MaxRandom);
        end;
    end; 
end;

procedure PrintMatrix(var matrix : SquareMatrix);
var
    i, j: Integer;
begin
    for i := 1 to MatrixSize do 
    begin
        for j := 1 to MatrixSize do
        begin
            Write(matrix[i, j]:3, '  ');
        end;
        WriteLn();         
    end;
end;

function GetMaxSecondaryDiagonalElement(var matrix : SquareMatrix) : MatrixElement;
var 
  i : Integer;
  max : MatrixElement;
begin
    max.row := 1;
    max.column := MatrixSize;
    max.value := matrix[max.row, max.column];
    for i := 2 to MatrixSize do
    begin
        if (max.value < matrix[i, MatrixSize - i + 1]) then
        begin
            max.row := i;
            max.column := MatrixSize - i + 1;
            max.value := matrix[max.row, max.column];
        end;
    end;
    GetMaxSecondaryDiagonalElement := max;
end;

function GetLowerMainDiagonalAverrage(var matrix : SquareMatrix) : Real;
var 
    i, j : Integer;
    sum : Integer;
begin
    sum := 0;
    for i := 1 to MatrixSize do
    begin
        for j := 1 to MatrixSize do
        begin
            if i > j then
            begin
                sum += matrix[i, j];
            end;
        end;
    end;
    GetLowerMainDiagonalAverrage := sum / (MatrixSize * MatrixSize / 2 - MatrixSize / 2);
end;

procedure ReplaceElement(var matrix : SquareMatrix; element : MatrixElement);
begin
    matrix[element.row, element.column] := element.value;
end;

end.